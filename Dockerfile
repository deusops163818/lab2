FROM alpine AS build

ARG NGINX_VERSION=1.24.0
ARG NGINX_DOWNLOAD_URL=https://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz

WORKDIR /tmp

RUN apk --no-cache --update add build-base gnupg pcre-dev wget zlib-dev linux-headers libressl-dev zlib-static

RUN set -x && wget -q $NGINX_DOWNLOAD_URL && tar -xf nginx-${NGINX_VERSION}.tar.gz && echo ${NGINX_VERSION}

WORKDIR /tmp/nginx-${NGINX_VERSION}


RUN ./configure --with-ld-opt='-static' --with-http_ssl_module && \
    make && \
    make install && \
    strip /usr/local/nginx/sbin/nginx

RUN ln -sf /dev/stdout /usr/local/nginx/logs/access.log && \
    ln -sf /dev/stderr /usr/local/nginx/logs/error.log


FROM scratch

COPY --from=build /usr/local/nginx /usr/local/nginx
COPY --from=build /etc/passwd /etc/group /etc/

STOPSIGNAL SIGQUIT

EXPOSE 80 443

ENTRYPOINT ["/usr/local/nginx/sbin/nginx"]
CMD ["-g", "daemon off;"]
