FROM alpine AS openssl

RUN apk add --no-cache openssl

WORKDIR /certificates

COPY ./generate-certificates.sh ./

CMD ["sh", "/certificates/generate-certificates.sh"]
